<?php

namespace Ttest\ProductNice\Ui\Component\Column;

class LikeDown extends \Magento\Ui\Component\Listing\Columns\Column
{
    protected $_productloader;

    protected $_likeFactory;

    /**
     * constructor
     *
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        \Magento\Catalog\Model\ProductFactory $productloader,
        \Ttest\ProductNice\Model\LikeFactory $likeFactory,
        array $components = [],
        array $data = []
    )
    {
        $this->_likeFactory = $likeFactory;
        $this->_productloader = $productloader;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }


    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['like_id'])) {
                    $product = $this->_productloader->create()->load($item['product_id']);

                    if(isset($product)){

                      $like = $this->_likeFactory->create();
                      $likes = $like->getCollection()

                          ->addFieldToFilter('product_id', $item['product_id'])
                          ->addFieldToFilter('value', '-1');


                      $item[$this->getData('name')] = count($likes);
                    }
                }
            }
        }
        return $dataSource;
    }
}
