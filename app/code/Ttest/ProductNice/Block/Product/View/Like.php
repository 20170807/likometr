<?php
namespace Ttest\ProductNice\Block\Product\View;

use Magento\Framework\View\Element\Template;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Category;

class Like extends \Magento\Catalog\Block\Product\View
{
	protected $_helperData;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Customer\Model\Session $customerSession,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
		\Ttest\ProductNice\Helper\Data $helperData,
        array $data = []
    ) {
		parent::__construct($context, $urlEncoder, $jsonEncoder, $string, $productHelper, $productTypeConfig, $localeFormat, $customerSession, $productRepository, $priceCurrency, $data);
        $this->_helperData = $helperData;
    }

    public function isCustomerLoggedIn()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        return $objectManager->get('Magento\Customer\Model\Session')->isLoggedIn();
    }

    public function setCurrentUrl()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $urlInterface = $objectManager->get('Magento\Framework\UrlInterface');
        $currentUrl = $urlInterface->getCurrentUrl();

        $objectManager->get('Magento\Customer\Model\Session')->setBeforeAuthUrl($currentUrl); 
    }

    public function getLikeValue()
    {
        $return = ['error' => true, 'value' => 0];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $customerSession = $objectManager->get('Magento\Customer\Model\Session');

        if ($customerSession->isLoggedIn()) {
            $product = $this->_coreRegistry->registry('product');
            $productId = $product->getId();

            $customerId = $customerSession->getCustomer()->getId();

            $like = $objectManager->get('\Ttest\ProductNice\Model\Like');
            $likes = $like->getCollection()
          	    ->addFieldToFilter('customer_id', $customerId)
          	    ->addFieldToFilter('product_id', $productId);

            if (count($likes) > 0) {
                $return['error'] = false;
                $return['value'] = $likes->getFirstItem()->getValue();
            }
        }

        return $return;
    }

    public function getLikeCss()
    {
        $return = false;

        $cssId = $this->_helperData->getMainOptionsConfig('css');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $likeCss = $objectManager->get('\Ttest\ProductNice\Model\LikeCss')
                ->getCollection()
                ->addFieldToFilter('css_id', $cssId);

        if (count($likeCss) > 0) {
            $return = $likeCss->getFirstItem()->getData();
        }

        return $return;
    }

    public function getAnimation()
    {
        return $this->_helperData->getMainOptionsConfig('animation');
    }

    public function getDivide()
    {
        return $this->_helperData->getMainOptionsConfig('divide');
    }

    public function getBorder()
    {
        return $this->_helperData->getMainOptionsConfig('border');
    }

    public function getActiveButton()
    {
        return $this->_helperData->getMainOptionsConfig('active_button');
    }

    public function getTextSize()
    {
        return $this->_helperData->getMainOptionsConfig('text_size');
    }

    public function getTextFont()
    {
        return $this->_helperData->getMainOptionsConfig('text_font');
    }

    public function getBackgroundColor()
    {
        return $this->_helperData->getMainOptionsConfig('background_color');
    }

    public function getLikeButtonShow()
    {
        return $this->_helperData->getLikeConfig('show');
    }

    public function getLikeTitleShow()
    {
        return $this->_helperData->getLikeConfig('title_show');
    }

    public function getLikeTitle()
    {
        $title = $this->_helperData->getLikeConfig('title');

        if (empty($title)) {
            $title = 'Like';
        }

        return __($title);
    }

    public function getLikeCounter()
    {
        return $this->_helperData->getLikeConfig('counter');
    }

    public function getDisLikeButtonShow()
    {
        return $this->_helperData->getDisLikeConfig('show');
    }

    public function getDisLikeTitleShow()
    {
        return $this->_helperData->getDisLikeConfig('title_show');
    }

    public function getDisLikeTitle()
    {
        $title = $this->_helperData->getDisLikeConfig('title');

        if (empty($title)) {
            $title = 'DisLike';
        }

        return __($title);
    }

    public function getDisLikeCounter()
    {
        return $this->_helperData->getDisLikeConfig('counter');
    }

    public function getCounterType()
    {
        return $this->_helperData->getCounterConfig('type');
    }

    public function getCounterForm()
    {
        return $this->_helperData->getCounterConfig('form');
    }

    public function getLikeNum($counterType = 'num', $counterForm = 'no')
    {
        $return = ['error' => true, 'value' => 0];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $product = $this->_coreRegistry->registry('product');
        $productId = $product->getId();

        $likeCount = $objectManager->get('\Ttest\ProductNice\Model\LikeCount');
        $likeCounts = $likeCount->getCollection()
                ->addFieldToFilter('product_id', $productId);

        if (count($likeCounts) > 0) {
            $return['error'] = false;
            $return['count_up_like'] = $likeCounts->getFirstItem()->getCountUpLike();
            $return['count_down_like'] = $likeCounts->getFirstItem()->getCountDownLike();

            // dummy - it needs to delete
            $return['count_up_like'] = 18500;
            $return['count_down_like'] = 700;

            $return = $this->makeCounter($return, $counterType, $counterForm);
        }

        return $return;
    }

    function makeCounter($return, $counterType, $counterForm)
    {
        // Deviding 1 000
        if ($counterType == 'num' && $counterForm == 'dev') {
            $return['count_up_like'] = number_format($return['count_up_like'], 0, '', ' ');
            $return['count_down_like'] = number_format($return['count_down_like'], 0, '', ' ');
        }

        // K&M 1K, 1M
        if ($counterType == 'num' && $counterForm == 'k_m') {
            $return['count_up_like'] = $this->niceNumber($return['count_up_like']);
            $return['count_down_like'] = $this->niceNumber($return['count_down_like']);
        }

        // Percents
        if ($counterType == 'per') { 
            $summ = $return['count_up_like'] + $return['count_down_like'];

            $return['count_up_like'] = round($return['count_up_like'] * 100 / $summ) . ' %';
            $return['count_down_like'] = round($return['count_down_like'] * 100 / $summ) . ' %';
        }

        return $return;
    }
    
    function niceNumber($n) 
    {
        // first strip any formatting;
        $n = (0 + str_replace(",", "", $n));

        // is this a number?
        if (!is_numeric($n)) {
            return false;
        }
        else if ($n > 1000000) {
            return round(($n / 1000000), 1) . ' M';
        }
        else if ($n < 1000000) {
            return round(($n / 1000), 1) . ' K';
        }

        return number_format($n);
    }

    function getCounterAnimation($counterType = 'num')
    {
        $return = '';

        if ($this->_helperData->getCounterConfig('animation') && $counterType == 'num') {
            $return = 'count';
        }

        return $return;
    }

    public function getBaseUrl() {
        return parent::getBaseUrl();
    }
    
    public function getProductId() {
        return parent::getProduct()->getId();
    }
}
