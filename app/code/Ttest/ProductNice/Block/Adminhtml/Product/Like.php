<?php
namespace Ttest\ProductNice\Block\Adminhtml\Product;

class Like extends \Magento\Backend\Block\Template implements \Magento\Framework\Data\Form\Element\Renderer\RendererInterface
{
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $element->getElementHtml();
    }

    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $baseUrl = parent::getBaseUrl();

        $html = "<div id='like_view_element' style=\"padding-left: 25%; height: 50px;\" data-base_url='{$baseUrl}'>&nbsp;</div>";

        return $html;
    }
}