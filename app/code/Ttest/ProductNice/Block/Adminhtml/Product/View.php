<?php

namespace Ttest\ProductNice\Block\Adminhtml\Product;

class View extends \Magento\Framework\View\Element\Template
{
    /**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry           $registry
     * @param array                                 $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    )
    {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    public function getUpLikesSize()
    {
          return $this->_coreRegistry->registry('up_likes_size');
    }

    public function getDownLikesSize()
    {
          return $this->_coreRegistry->registry('down_likes_size');
    }

    public function getUpLikesEmails()
    {
          return $this->_coreRegistry->registry('up_likes_emails');
    }

    public function getDownLikesEmails()
    {
          return $this->_coreRegistry->registry('down_likes_emails');
    }

    protected function _prepareLayout()
    {

    }
}
