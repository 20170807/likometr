<?php
namespace Ttest\ProductNice\Block\Like;

class View extends \Magento\Framework\View\Element\Template
{
    protected $_request;

    protected $_helperData;

    public function __construct(
            \Magento\Framework\View\Element\Template\Context $context,
            \Ttest\ProductNice\Helper\Data $helperData
    ) {
        $this->_request = $context->getRequest();
        $this->_helperData = $helperData;
        parent::__construct($context);
    }

    public function isCustomerLoggedIn()
    {
        return true;
    }

    public function getLikeValue()
    {
        $return = ['error' => false, 'value' => 1];

        return $return;
    }

    public function getLikeCss()
    {
        $return = false;

        $cssId = (int) $this->_request->getParam('likometr_main_options_css');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $customerSession = $objectManager->get('Magento\Customer\Model\Session');

        $likeCss = $objectManager->get('\Ttest\ProductNice\Model\LikeCss')
                ->getCollection()
                ->addFieldToFilter('css_id', $cssId);

        if (count($likeCss) > 0) {
            $return = $likeCss->getFirstItem()->getData();
        }

        return $return;
    }

    public function getAnimation()
    {
        return $this->_request->getParam('likometr_main_options_animation');
    }

    public function getDivide()
    {
        return (bool) $this->_request->getParam('likometr_main_options_divide');
    }

    public function getBorder()
    {
        return (bool) $this->_request->getParam('likometr_main_options_border');
    }

    public function getActiveButton()
    {
        return (bool) $this->_request->getParam('likometr_main_options_active_button');
    }

    public function getTextSize()
    {
        return $this->_request->getParam('likometr_main_options_text_size');
    }

    public function getTextFont()
    {
        return $this->_request->getParam('likometr_main_options_text_font');
    }

    public function getBackgroundColor()
    {
        return $this->_request->getParam('likometr_main_options_background_color');
    }

    public function getLikeButtonShow()
    {
        return (bool) $this->_request->getParam('likometr_like_show');
    }

    public function getLikeTitleShow()
    {
        return (bool) $this->_request->getParam('likometr_like_title_show');
    }

    public function getLikeTitle()
    {
        $title = $this->_request->getParam('likometr_like_title');

        if (empty($title)) {
            $title = 'Like';
        }

        return __($title);
    }

    public function getLikeCounter()
    {
        return (bool) $this->_request->getParam('likometr_like_counter');
    }

    public function getDisLikeButtonShow()
    {
        return (bool) $this->_request->getParam('likometr_dislike_show');
    }

    public function getDisLikeTitleShow()
    {
        return (bool) $this->_request->getParam('likometr_dislike_title_show');
    }

    public function getDisLikeTitle()
    {
        $title = $this->_request->getParam('likometr_dislike_title');

        if (empty($title)) {
            $title = 'DisLike';
        }

        return __($title);
    }

    public function getDisLikeCounter()
    {
        return (bool) $this->_request->getParam('likometr_dislike_counter');
    }

    public function getCounterType()
    {
        return $this->_request->getParam('likometr_counter_type');
    }

    public function getCounterForm()
    {
        return $this->_request->getParam('likometr_counter_form');
    }

    public function getLikeNum($counterType = 'num', $counterForm = 'no')
    {
        $return['error'] = false;

        $return['count_up_like'] = 18500;
        $return['count_down_like'] = 700;

        $return = $this->makeCounter($return, $counterType, $counterForm);

        return $return;
    }

    function makeCounter($return, $counterType, $counterForm)
    {
        // Deviding 1 000
        if ($counterType == 'num' && $counterForm == 'dev') {
            $return['count_up_like'] = number_format($return['count_up_like'], 0, '', ' ');
            $return['count_down_like'] = number_format($return['count_down_like'], 0, '', ' ');
        }

        // K&M 1K, 1M
        if ($counterType == 'num' && $counterForm == 'k_m') {
            $return['count_up_like'] = $this->niceNumber($return['count_up_like']);
            $return['count_down_like'] = $this->niceNumber($return['count_down_like']);
        }

        // Percents
        if ($counterType == 'per') { 
            $summ = $return['count_up_like'] + $return['count_down_like'];

            $return['count_up_like'] = round($return['count_up_like'] * 100 / $summ) . ' %';
            $return['count_down_like'] = round($return['count_down_like'] * 100 / $summ) . ' %';
        }

        return $return;
    }
    
    function niceNumber($n) 
    {
        // first strip any formatting;
        $n = (0 + str_replace(",", "", $n));

        // is this a number?
        if (!is_numeric($n)) {
            return false;
        }
        else if ($n > 1000000) {
            return round(($n / 1000000), 1) . ' M';
        }
        else if ($n < 1000000) {
            return round(($n / 1000), 1) . ' K';
        }

        return number_format($n);
    }

    function getCounterAnimation($counterType = 'num')
    {
        $return = '';

        if ($this->_request->getParam('likometr_counter_animation') && $counterType == 'num') {
            $return = 'count';
        }

        return $return;
    }

    public function getBaseUrl() {
        return parent::getBaseUrl();
    }
    
    public function getProductId() {
        return false;
    }
}
