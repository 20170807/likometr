<?php

namespace Ttest\ProductNice\Model\ResourceModel\Like;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Ttest\ProductNice\Model\Like','Ttest\ProductNice\Model\ResourceModel\Like');
    }
}