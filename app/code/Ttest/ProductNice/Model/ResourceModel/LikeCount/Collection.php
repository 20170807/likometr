<?php

namespace Ttest\ProductNice\Model\ResourceModel\LikeCount;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Ttest\ProductNice\Model\LikeCount','Ttest\ProductNice\Model\ResourceModel\LikeCount');
    }

    function joinLikeTable()
    {
        $this->main_table = 'main_table';
        $this->product_nice_like_table = 'cc_product_nice_like';
        $this->customer_table = 'customer_entity';
        $this->getSelect()
            ->join(
                ['like' => $this->product_nice_like_table],
                "{$this->main_table}.product_id = like.product_id"
            )
            ->join(
                ['customer' => $this->customer_table],
                "like.customer_id = customer.entity_id"
            )
            ->reset(\Zend_Db_Select::COLUMNS)
            ->columns(["{$this->main_table}.*", 'like.*', 'customer.email']);
    }
}