<?php

namespace Ttest\ProductNice\Model\ResourceModel\LikeCss;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Ttest\ProductNice\Model\LikeCss','Ttest\ProductNice\Model\ResourceModel\LikeCss');
    }
}