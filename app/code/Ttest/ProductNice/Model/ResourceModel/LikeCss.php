<?php

namespace Ttest\ProductNice\Model\ResourceModel;

class LikeCss extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('cc_product_nice_like_css', 'css_id');
    }
}