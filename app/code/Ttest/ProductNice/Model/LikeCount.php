<?php

namespace Ttest\ProductNice\Model;

class LikeCount extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'cc_product_nice_like_count';
    
    protected function _construct()
    {
        $this->_init('Ttest\ProductNice\Model\ResourceModel\LikeCount');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}	
