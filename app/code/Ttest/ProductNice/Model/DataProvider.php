<?php
namespace Ttest\ProductNice\Model;

use Ttest\ProductNice\Model\ResourceModel\LikeCount\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $likeCountCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $likeCountCollectionFactory->create();
        $this->collection->joinLikeTable();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }

        $likeCounts = $this->collection->getData();

        $count_up_like_emails = '';
        $count_down_like_emails = '';

        foreach ($likeCounts as $likeCount) {
            if ($likeCount['value'] == -1) {
                $count_down_like_emails .= "{$likeCount['email']},";
            } else {
                $count_up_like_emails .= "{$likeCount['email']},";
            }
        }

        $this->_loadedData[$likeCount['count_id']] = $likeCount;

        $this->_loadedData[$likeCount['count_id']]['count_up_like_emails'] = trim($count_up_like_emails, ', ');
        $this->_loadedData[$likeCount['count_id']]['count_down_like_emails'] = trim($count_down_like_emails, ', ');

        return $this->_loadedData;
    }
}
