<?php
namespace Ttest\ProductNice\Model\Config\Source;

class Type implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'num', 'label' => __('Numbers')],
            ['value' => 'per', 'label' => __('Percents')]
        ];
    }
}
