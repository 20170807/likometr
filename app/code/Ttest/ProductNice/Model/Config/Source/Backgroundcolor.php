<?php
namespace Ttest\ProductNice\Model\Config\Source;

class Backgroundcolor implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 0, 'label' => __('Select Text Size ...')],
            ['value' => 'Arial', 'label' => 'Arial'],
            ['value' => 'Helvetica', 'label' => 'Helvetica'],
            ['value' => 'Times New Roman', 'label' => 'Times New Roman'],
            ['value' => 'Times', 'label' => 'Times'],
            ['value' => 'Courier New', 'label' => 'Courier New'],
            ['value' => 'Courier', 'label' => 'Courier'],
            ['value' => 'Verdana', 'label' => 'Verdana'],
            ['value' => 'Georgia', 'label' => 'Georgia'],
            ['value' => 'Palatino', 'label' => 'Palatino'],
            ['value' => 'Garamond', 'label' => 'Garamond'],
            ['value' => 'Bookman', 'label' => 'Bookman'],
            ['value' => 'Comic Sans MS', 'label' => 'Comic Sans MS'],
            ['value' => 'Trebuchet MS', 'label' => 'Trebuchet MS'],
            ['value' => 'Arial Black', 'label' => 'Arial Black'],
            ['value' => 'Impact', 'label' => 'Impact']

        ];
    }
}
