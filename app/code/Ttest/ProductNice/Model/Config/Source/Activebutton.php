<?php
namespace Ttest\ProductNice\Model\Config\Source;

class Activebutton implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 0, 'label' => __('All button')],
            ['value' => 1, 'label' => __('Only like element')]
        ];
    }
}
