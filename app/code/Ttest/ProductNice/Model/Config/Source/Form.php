<?php
namespace Ttest\ProductNice\Model\Config\Source;

class Form implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'no', 'label' => __('No Deviding 1000 ')],
            ['value' => 'dev', 'label' => __('Deviding 1 000 ')],
            ['value' => 'k_m', 'label' => __('K&M 1K, 1M ')]
        ];
    }
}
