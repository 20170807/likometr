<?php
namespace Ttest\ProductNice\Model\Config\Source;

class Css implements \Magento\Framework\Option\ArrayInterface
{
    protected $_collectionFactory;

    protected $_options;

    public function __construct(
        \Ttest\ProductNice\Model\ResourceModel\LikeCss\CollectionFactory $collectionFactory
    ) {
        $this->_collectionFactory = $collectionFactory;
    }

    public function toOptionArray()
    {
        if ($this->_options === null) {
            $collection = $this->_collectionFactory->create();

            $this->_options = [['value' => 0, 'label' => __('Please select ...')]];

            foreach ($collection as $css) {
                $this->_options[] = [
                    'label' => $css->getName(),
                    'value' => $css->getCssId()
                ];
            }
        }

        return $this->_options;
    }
}
