<?php
namespace Ttest\ProductNice\Model\Config\Source;

class Animation implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 0, 'label' => __('Please select ...')],
            ['value' => 'blind', 'label' => __('Blind')],
            ['value' => 'bounce', 'label' => __('Bounce')],
            ['value' => 'clip', 'label' => __('Clip')],
            ['value' => 'drop', 'label' => __('Drop')],
            ['value' => 'explode', 'label' => __('Explode')],
            ['value' => 'fade', 'label' => __('Fade')],
            ['value' => 'fold', 'label' => __('Fold')],
            ['value' => 'highlight', 'label' => __('Highlight')],
            ['value' => 'puff', 'label' => __('Puff')],
            ['value' => 'pulsate', 'label' => __('Pulsate')],
            ['value' => 'scale', 'label' => __('Scale')],
            ['value' => 'shake', 'label' => __('Shake')],
            ['value' => 'size', 'label' => __('Size')],
            ['value' => 'slide', 'label' => __('Slide')]
        ];
    }
}
