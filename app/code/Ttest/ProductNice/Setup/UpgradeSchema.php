<?php

namespace Ttest\ProductNice\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements  UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0)
        {
            $table = $setup->getConnection()
            ->newTable(
                $setup->getTable('cc_product_nice_like_count')
            )
            ->addColumn(
                'count_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Count ID'
            )
            ->addColumn(
                'product_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Product ID'
            )
            ->addColumn(
                'count_up_like',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Count Up Like'
            )
            ->addColumn(
                'count_down_like',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Count Down Like'
            )
            ->setComment(
                'Create Count Like Table'
            );
                $setup->getConnection()->createTable($table);
        }

        if (version_compare($context->getVersion(), '1.0.2') < 0)
        {
            $table = $setup->getConnection()
            ->newTable(
                $setup->getTable('cc_product_nice_like_css')
            )
            ->addColumn(
                'css_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'CSS ID'
            )
            ->addColumn(
                'like_on',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Like On CSS'
            )
            ->addColumn(
                'like_off',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Like Off CSS'
            )
            ->addColumn(
                'dislike_on',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Dislike On CSS'
            )
            ->addColumn(
                'dislike_off',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Dislike Off CSS'
            )
            ->setComment(
                'Create CSS Like table'
            );

            $setup->getConnection()->createTable($table);
        }

        if (version_compare($context->getVersion(), '1.0.4') < 0)
        {
            $setup->getConnection()->addColumn(
                 $setup->getTable('cc_product_nice_like_css'),
                'name',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => false,
                    'default' => '',
                    'LENGTH' =>255,
                    'comment' => 'Name',
                    'after' => 'css_id'
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.6') < 0)
        {
            $setup->getConnection()->addColumn(
                 $setup->getTable('cc_product_nice_like_css'),
                'like_hover',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => false,
                    'comment' => 'Like Hover',
                    'after' => 'like_off'
                ]
            );

            $setup->getConnection()->addColumn(
                 $setup->getTable('cc_product_nice_like_css'),
                'dislike_hover',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => false,
                    'comment' => 'DisLike Hover',
                    'after' => 'dislike_off'
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.7') < 0)
        {
            $setup->getConnection()->addColumn(
                 $setup->getTable('cc_product_nice_like_css'),
                'width',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => false,
                    'comment' => 'Width Like / Dislike Element',
                    'after' => 'name'
                ]
            );

            $setup->getConnection()->addColumn(
                 $setup->getTable('cc_product_nice_like_css'),
                'height',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => false,
                    'comment' => 'Height Like / Dislike Element',
                    'after' => 'Width'
                ]
            );
        }

        $setup->endSetup();
    }
}
