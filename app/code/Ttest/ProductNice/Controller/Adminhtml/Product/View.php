<?php

namespace Ttest\ProductNice\Controller\Adminhtml\Product;

use Magento\Framework\Controller\ResultFactory;

class View extends \Magento\Backend\App\Action
{
    protected $_productloader;

    protected $_likeFactory;

    protected $_customer;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Catalog\Model\ProductFactory $productloader,
        \Ttest\ProductNice\Model\LikeFactory $likeFactory,
        \Magento\Customer\Model\Customer $customer
    )
    {
        $this->_customer = $customer;
        $this->_likeFactory = $likeFactory;
        $this->_productloader = $productloader;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    public function execute()
    {
        $productId = (int) $this->getRequest()->getParam('product_id');

        $like = $this->_likeFactory->create();
        $upLikes = $like->getCollection()
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('value', '1');

        $upLikesSize = $upLikes->getSize();

        $upLikesEmails = [];
        foreach ($upLikes as $upLike) {
            $customerId = $upLike->getCustomerId();
            $customer = $this->_customer->load($customerId);
            $upLikesEmails[] = $customer->getEmail();
        }

        $like = $this->_likeFactory->create();
        $downLikes = $like->getCollection()
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('value', '-1');

        $downLikesSize = $downLikes->getSize();

        $downLikesEmails = [];
        foreach ($downLikes as $downLike) {
            $customerId = $downLike->getCustomerId();
            $customer = $this->_customer->load($customerId);
            $downLikesEmails[] = $customer->getEmail();
        }

        $this->_coreRegistry->register('up_likes_size', $upLikesSize);
        $this->_coreRegistry->register('down_likes_size', $downLikesSize);

        $this->_coreRegistry->register('up_likes_emails', $upLikesEmails);
        $this->_coreRegistry->register('down_likes_emails', $downLikesEmails);

        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $resultPage->getConfig()->getTitle()->prepend(__('Product Like / Dislike View'));
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Ttest_ProductNice::main_menu');
    }
}
