<?php
namespace Ttest\ProductNice\Controller\Adminhtml\Product;

//use Magento\Backend\App\Action;

class Index extends \Magento\Backend\App\Action
{
    protected $_resultPageFactory = false;
    protected $_resultPage = null;

    public function __construct(
      	\Magento\Backend\App\Action\Context $context,
      	\Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
      	parent::__construct($context);
      	$this->_resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
      $this->_setPageData();
          return $this->getResultPage();
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Ttest_ProductNice::products_list');
    }

    public function getResultPage()
    {
        if (is_null($this->_resultPage)) {
            $this->_resultPage = $this->_resultPageFactory->create();
        }
        return $this->_resultPage;
    }

    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Ttest_ProductNice::products_list');

        return $this;
    }
}
