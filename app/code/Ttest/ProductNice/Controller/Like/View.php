<?php
namespace Ttest\ProductNice\Controller\Like;

class View extends \Magento\Framework\App\Action\Action
{
    protected $_layoutFactory;

	protected $_helperData;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Ttest\ProductNice\Helper\Data $helperData
    ) {
        $this->_helperData = $helperData;
        $this->_layoutFactory = $layoutFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $likeCss = $this->_helperData->getMainOptionsConfig('css');

        $output = $this->_layoutFactory->create()
            ->createBlock('Ttest\ProductNice\Block\Like\View')
            ->setTemplate('Ttest_ProductNice::product/view/like.phtml')
            ->toHtml();
        $this->getResponse()->setBody($output);
    }
}
