<?php
namespace Ttest\ProductNice\Controller\Like;

class Email extends \Magento\Framework\App\Action\Action
{
	public function __construct(
		\Magento\Framework\App\Action\Context $context
	) {
		parent::__construct($context);
	}

	public function execute()
	{
		$receiverInfo = [
			'name' => 'Reciver Name',
			'email' => 'sergey.dovbenko@gmail.com'
		];

		$senderInfo = [
			'name' => 'Sender Name',
			'email' => 'sender@addess.com',
		];

		// added temp vars for template
		$emailTemplateVariables = array();
		$emailTemplateVariables['myvar1'] = 'variablevalue1';
		$emailTemplateVariables['myvar2'] = 'variablevalue2';
		$emailTemplateVariables['myvar3'] = 'variablevalue3';
		$emailTemplateVariables['myvar4'] = 'variablevalue4';
		$emailTemplateVariables['myvar5'] = 'variablevalue5';

		$this->_objectManager->get('Ttest\ProductNice\Helper\Email')->yourCustomMailSendMethod(
			$emailTemplateVariables,
			$senderInfo,
			$receiverInfo
		);

		die('Sent');
	}
}