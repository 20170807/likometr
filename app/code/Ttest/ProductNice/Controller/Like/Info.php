<?php
namespace Ttest\ProductNice\Controller\Like;

class Info extends \Magento\Framework\App\Action\Action
{
    protected $likeFactory;

    protected $likeCountFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Ttest\ProductNice\Model\LikeFactory $likeFactory,
        \Ttest\ProductNice\Model\LikeCountFactory $likeCountFactory
    ) {
        $this->likeCountFactory = $likeCountFactory;
        $this->likeFactory = $likeFactory;
        parent::__construct($context);
    }


    public function execute()
    {
        $productId = $this->getRequest()->getParam('product_id');
        $return = ['error' => true];

        $customerSession = $this->_objectManager->get('Magento\Customer\Model\Session');

        if($customerSession->isLoggedIn()) {
            $customerId = $customerSession->getCustomer()->getId();

            $like = $this->likeFactory->create();
            $likes = $like->getCollection()
          	    ->addFieldToFilter('customer_id', $customerId)
          	    ->addFieldToFilter('product_id', $productId);

            if (count($likes) > 0) {
                $return['error'] = false;
                $return['value'] = $likes->getFirstItem()->getValue();
            }

            $likeCount = $this->likeCountFactory->create();
            $likeCounts = $likeCount->getCollection()
                    ->addFieldToFilter('product_id', $productId);

            if (count($likeCounts) > 0) {
                $return['error'] = false;
                $return['count_up_like'] = $likeCounts->getFirstItem()->getCountUpLike();
                $return['count_down_like'] = $likeCounts->getFirstItem()->getCountDownLike();

                $return['count_up_like'] = 18500;
                $return['count_down_like'] = 700;
            }

        }
        else{
            $return['message'] = 'Please Sign In.';
        }

        $this->getResponse()->representJson(
            $this->_objectManager->get(\Magento\Framework\Json\Helper\Data::class)->jsonEncode($return)
        );
    }
}
