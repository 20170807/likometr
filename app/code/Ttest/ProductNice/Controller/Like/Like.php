<?php

namespace Ttest\ProductNice\Controller\Like;

class Like extends \Magento\Framework\App\Action\Action
{
    protected $_likeFactory;

    protected $_likeCountFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Ttest\ProductNice\Model\LikeFactory $likeFactory,
        \Ttest\ProductNice\Model\LikeCountFactory $likeCountFactory
    ) {
        $this->_likeFactory = $likeFactory;
        $this->_likeCountFactory = $likeCountFactory;
        parent::__construct($context);
    }


    public function execute()
    {
        $productId = $this->getRequest()->getParam('product_id');
        $return = ['error' => true];

        $customerSession = $this->_objectManager->get('Magento\Customer\Model\Session');
        if($customerSession->isLoggedIn()) {
            $customerId = $customerSession->getCustomer()->getId();

            $like = $this->_likeFactory->create();

            $likes = $like->getCollection()
                ->addFieldToFilter('customer_id', $customerId)
                ->addFieldToFilter('product_id', $productId);

            $addCount = $addReversed = false;

            if (count($likes) == 0) { // there is not any likes in DB
                // add like to like table
                $like->setData('customer_id', $customerId)
                    ->setData('product_id', $productId)
                    ->setData('value', 1)
                    ->save();

                // if new like add couner + 1
                $addCount = true;
            } else {
                //set all avaleable likes for this product and customer.
                //not posible but to be sure...
                foreach($likes as $like) {
                    // if change value add couner + 1
                    $value = $like->getValue();
                    if($value == -1 || $value == 0)
                        $addCount = true;

                    // in case product has another like (down)
                    if($value == -1)
                      $addReversed = true;

                    $like->setData('value', 1)->save();
                }
            }

            // so add counter + 1
            if ($addCount) {
                $likeCount = $this->_likeCountFactory->create();
                $likeCounts = $likeCount->load($productId, 'product_id');

                $countUpLike = $likeCounts->getCountUpLike();
                $countUpLike = $countUpLike + 1;

                $countDownLike = $likeCounts->getCountDownLike();
                if ($addReversed)
                  $countDownLike = $countDownLike - 1;

                $likeCounts->setData('product_id', $productId)
                    ->setData('count_down_like', $countDownLike)
                    ->setData('count_up_like', $countUpLike)
                    ->save();
            }

             $return['error'] = false;
        }
        else{
            $return['message'] = 'Please Sign In.';
        }

        $this->getResponse()->representJson(
            $this->_objectManager->get(\Magento\Framework\Json\Helper\Data::class)->jsonEncode($return)
        );
    }
}
