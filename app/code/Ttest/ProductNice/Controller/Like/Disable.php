<?php
namespace Ttest\ProductNice\Controller\Like;

class Disable extends \Magento\Framework\App\Action\Action
{
    protected $likeFactory;

    protected $_likeCountFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Ttest\ProductNice\Model\LikeFactory $likeFactory,
        \Ttest\ProductNice\Model\LikeCountFactory $likeCountFactory
    ) {
        $this->likeFactory = $likeFactory;
        $this->_likeCountFactory = $likeCountFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $productId = $this->getRequest()->getParam('product_id');
        $return = ['error' => true];

        $customerSession = $this->_objectManager->get('Magento\Customer\Model\Session');
        if($customerSession->isLoggedIn()) {
            $customerId = $customerSession->getCustomer()->getId();

              $like = $this->likeFactory->create();

            $likes = $like->getCollection()
                ->addFieldToFilter('customer_id', $customerId)
                ->addFieldToFilter('product_id', $productId);

            $disableUp = 0;
            $disableDown = 0;

            if (count($likes) == 0) {// there is not any likes in DB
                $like->setData('customer_id', $customerId)
                    ->setData('product_id', $productId)
                    ->setData('value', 0)
                    ->save();
            } else {
                //set all avaleable likes for this product and customer.
                foreach($likes as $like) {
                    // if change value minus couner: -1
                    $value = $like->getValue();

                    if($value == 1)
                        $disableUp = 1;
                    else if ($value == -1)
                        $disableDown = 1;

                    $like->setData('value', 0)->save();
                }
            }

            // so minus counter: -1
            if ($disableUp == 1 || $disableDown == 1) {
                $likeCount = $this->_likeCountFactory->create();
                $likeCounts = $likeCount->load($productId, 'product_id');

                $countDownLike = $likeCounts->getCountDownLike();
                $countDownLike = $countDownLike - $disableDown;

                $countUpLike = $likeCounts->getCountUpLike();
                $countUpLike = $countUpLike - $disableUp;

                $likeCounts->setData('product_id', $productId)
                    ->setData('count_down_like', $countDownLike)
                    ->setData('count_up_like', $countUpLike)
                    ->save();
            }

            $return['error'] = false;
        }
        else{
            $return['message'] = 'Please Sign In.';
        }

        $this->getResponse()->representJson(
                  $this->_objectManager->get(\Magento\Framework\Json\Helper\Data::class)->jsonEncode($return)
        );
    }
}
