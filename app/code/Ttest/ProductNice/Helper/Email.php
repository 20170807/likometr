<?php
namespace Ttest\ProductNice\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;


class Email extends AbstractHelper
{
	const XML_PATH_LIKOMETR_EMAIL_TEMPLATE = 'likometr/main_options/email_template';

	protected $_storeManager;

	protected $_inlineTranslation;

	protected $_transportBuilder;

	protected $_temp_id;

	public function __construct(
		\Magento\Framework\App\Helper\Context $context,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
		\Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
	) {
		parent::__construct($context);
		$this->_storeManager = $storeManager;
		$this->_inlineTranslation = $inlineTranslation;
		$this->_transportBuilder = $transportBuilder; 
	}

	protected function getConfigValue($path, $storeId)
	{
		return $this->scopeConfig->getValue(
			$path,
			ScopeInterface::SCOPE_STORE,
			$storeId
		);
	}

	public function getStore()
	{
		return $this->_storeManager->getStore();
	}

	public function getTemplateId($xmlPath)
	{
		return $this->getConfigValue($xmlPath, $this->getStore()->getStoreId());
	}

	public function generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo)
	{
		$this->_transportBuilder->setTemplateIdentifier($this->_temp_id);
		$this->_transportBuilder->setTemplateOptions([
						'area' => \Magento\Framework\App\Area::AREA_ADMINHTML,
						'store' => $this->_storeManager->getStore()->getId()
					]);

		$this->_transportBuilder->setTemplateVars($emailTemplateVariables);
		$this->_transportBuilder->setFrom($senderInfo);
		$this->_transportBuilder->addTo($receiverInfo['email'], $receiverInfo['name']);
		

		return $this;
	}

	public function yourCustomMailSendMethod($emailTemplateVariables, $senderInfo, $receiverInfo)
	{
		$this->_temp_id = $this->getTemplateId(self::XML_PATH_LIKOMETR_EMAIL_TEMPLATE);
		$this->_inlineTranslation->suspend();
		$this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo);
		$transport = $this->_transportBuilder->getTransport();
		$transport->sendMessage();
		$this->_inlineTranslation->resume();
	}
}