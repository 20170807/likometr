<?php
namespace Ttest\ProductNice\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const XML_PATH_LIKOMETR = 'likometr/';

    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    public function getMainOptionsConfig($code, $storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_LIKOMETR .'main_options/'. $code, $storeId);
    }

    public function getLikeConfig($code, $storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_LIKOMETR .'like/'. $code, $storeId);
    }

    public function getDisLikeConfig($code, $storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_LIKOMETR .'dislike/'. $code, $storeId);
    }

    public function getCounterConfig($code, $storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_LIKOMETR .'counter/'. $code, $storeId);
    }
}